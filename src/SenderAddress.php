<?php
/**
 * Box.
 */

namespace WpDesk\WooCommerce\ShippingMethod;

use WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress\AbstractSenderAddress;

/**
 * Settings for box from saved settings.
 *
 * @package WpDesk\WooCommerce\ShippingMethod
 */
class SenderAddress extends AbstractSenderAddress {

    /**
     * @param array $sender_address_array .
     *
     * @return SenderAddress
     */
    static public function create_from_array( array $sender_address_array ) {
        $address = new SenderAddress();
        if ( isset( $sender_address_array[ self::ADDRESS_ID ] ) ) {
            $address->set_address_id( $sender_address_array[ self::ADDRESS_ID ] );
        }
	    if ( isset( $sender_address_array[ self::COMPANY ] ) ) {
		    $address->set_company( $sender_address_array[ self::COMPANY ] );
	    }
        if ( isset( $sender_address_array[ self::NAME ] ) ) {
            $address->set_name( $sender_address_array[ self::NAME ] );
        }
	    if ( isset( $sender_address_array[ self::ADDRESS ] ) ) {
		    $address->set_address( $sender_address_array[ self::ADDRESS ] );
	    }
	    if ( isset( $sender_address_array[ self::POSTAL_CODE ] ) ) {
		    $address->set_postal_code( $sender_address_array[ self::POSTAL_CODE ] );
	    }
	    if ( isset( $sender_address_array[ self::CITY ] ) ) {
		    $address->set_city( $sender_address_array[ self::CITY ] );
	    }
	    if ( isset( $sender_address_array[ self::PHONE ] ) ) {
		    $address->set_phone( $sender_address_array[ self::PHONE ] );
	    }
	    if ( isset( $sender_address_array[ self::EMAIL ] ) ) {
		    $address->set_email( $sender_address_array[ self::EMAIL ] );
	    }
        return $address;
    }

	/**
	 * Create addresses from settings.
	 *
	 * @param string $sender_address_settings JSON string.
	 *
	 * @return SenderAddress[]
	 */
	public static function create_sender_addresses_from_settings( $sender_address_settings ) {
		$sender_addresses = [];
		foreach ( json_decode( $sender_address_settings, true ) as $box_setting ) {
			$sender_addresses[] = SenderAddress::create_from_array( $box_setting );
		}

		return $sender_addresses;
	}

}
