<?php
/**
 * Labels.
 *
 * @package WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress
 */

namespace WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress;

/**
 * Labels for REACT component.
 */
class Labels {

    public $address_id;
	public $company;
	public $name;
	public $address;
	public $postal_code;
	public $city;
	public $phone;
	public $email;

	public $button_delete;
	public $button_add;

	public $non_unique_id;

	public function __construct() {
		$this->address_id  = __( 'Identifier', 'wp-settings-field-sender-address' );
		$this->company     = __( 'Company', 'wp-settings-field-sender-address' );
		$this->name        = __( 'Name', 'wp-settings-field-sender-address' );
		$this->address     = __( 'Address', 'wp-settings-field-sender-address' );
		$this->postal_code = __( 'Postal code', 'wp-settings-field-sender-address' );
		$this->city        = __( 'City', 'wp-settings-field-sender-address' );
		$this->phone       = __( 'Phone', 'wp-settings-field-sender-address' );
		$this->email       = __( 'Email', 'wp-settings-field-sender-address' );

		$this->button_add    = __( 'Add', 'wp-settings-field-sender-address' );
		$this->button_delete = __( 'Delete', 'wp-settings-field-sender-address' );

		$this->non_unique_id = __( 'Identifier is not unique!', 'wp-settings-field-sender-address' );
	}

}
