import React, { Component } from 'react';
import SenderAddressTable from "./sender-address-table.jsx";

/**
 * Can display sender address in div.
 * User can add multiple sender addresses.
 */
export default class SenderAddress extends React.Component {

    /**
     * @param props
     */
    constructor (props) {
        super(props);
        /**
         * @type {{addresses: array, name: string, labels: string}}
         */
        this.state = {
            addresses: JSON.parse(props.addresses),
            name: props.name,
            labels: JSON.parse(props.labels),
        };
    }

    /**
     * @returns {*}
     */
    render () {
        return (
            <div>
                <SenderAddressTable
                    addresses={this.state.addresses}
                    name={this.state.name}
                    labels={this.state.labels}
                />
            </div>
        )
    }
}
