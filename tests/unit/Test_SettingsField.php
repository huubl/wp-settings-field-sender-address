<?php

use WP_Mock\Tools\TestCase;
use WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress\Labels;
use WpDesk\WooCommerce\ShippingMethod\Fields\SenderAddress\SettingsField;

class Test_SettingsField extends TestCase {

    /**
     * Set up.
     */
    public function setUp() {
        \WP_Mock::setUp();
    }

    /**
     * Tear down.
     */
    public function tearDown() {
        \WP_Mock::tearDown();
    }

    /**
     * Test posted value to JSON.
     */
    public function test_get_field_posted_value_as_json() {

        $settings_field = new SettingsField( 'boxes' );

        $this->assertEquals( '[]', $settings_field->get_field_posted_value_as_json( null ), 'Null posted value should be empty array in JSON!' );

        $this->assertEquals( '{"name":"value"}', $settings_field->get_field_posted_value_as_json( array( 'name' => 'value' ) ), 'Posted array value should be array in JSON!' );

    }

    /**
     * Test render.
     */
    public function test_render() {

        $field_name = 'boxes';
        $settings_field = new SettingsField( $field_name );

        \WP_Mock::passthruFunction( 'wp_kses_post' );

        $json_values = '[{"name":"value"}]';

        $this->expectOutputString('<tr valign="top">
    <th scope="row" class="titledesc">
        <label for="boxes">Field Title<span>Tooltip</span></label>
    </th>
    <td class="forminp">
        <fieldset
            class="settings-field-sender-address"
            id="boxes_fieldset"
            data-value="' . $json_values . '"
            data-name="' . $field_name . '"
            data-labels="' . esc_attr( json_encode( new Labels(), JSON_FORCE_OBJECT ) ) . '"
        >
        </fieldset>
    </td>
</tr>
');

        $settings_field->render(
            'Field Title',
            '<span>Tooltip</span>',
            $json_values,
            null
        );

    }

}
